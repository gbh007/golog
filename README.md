# Пакет логирования golog

## Описание

### Возможности

* потоки логирования в файл и стандартный поток ошибок
* потоки логирования в стандартный поток ошибок
* использование произвольных потоков логирования
* переключения потока на лету
* перемещение старых логов при использовании логирования в файл
* уровни логирования
  * Отладки
  * Информационный
  * Предупреждений
  * Ошибок
  * Критический
  * Паники
* переключение уровня логирования на лету
* порождение дочерних логеров
  * дочернии логеры являются копией родителя за исключением префикса (задается при создании потомка)
  * изменение дочернего логера не затрагивает родителя (при изменении потока в потомке в родителе он не изменится)

## Установка

Скачать и установить командой  
>`go get gitlab.com/gbh007/golog`

## Использование

Простое логирование (в стандартный поток ошибок)

```go
package main

import (
    "gitlab.com/gbh007/golog"
)

func main() {
    golog.Critical("start")
    golog.Info("hello")
}
```

Создание дочернего лога

```go
package main

import (
    "gitlab.com/gbh007/golog"
)

func main() {
    newLog := golog.New("I am new log")
    newLog.Info("hello")
}
```

или

```go
package main

import (
    "gitlab.com/gbh007/golog"
)

func main() {
    myLog := golog.NewWriterLog(os.Stderr,"my log", golog.LogLevelDebug)
    newLog := myLog.New("I am new log")
    newLog.Info("hello")
}
```

Создание лога на произвольном потоке

```go
package main

import (
    "gitlab.com/gbh007/golog"
    "io"
    "os"
)

func main() {
    file, _ := os.Create("out.txt")
    file1, _ := os.Create("out1.txt")
    file2, _ := os.Create("out2.txt")
    myLog:=golog.NewWriterLog(io.MultiWriter(file, file1, file2, os.Stderr),"my log", golog.LogLevelDebug)
    myLog.Critical("hello my log!")
}
```

Создание лога в файловый поток и стандартный поток ошибок

```go
package main

import (
    "gitlab.com/gbh007/golog"
)

func main() {
    fileLog, err:=golog.NewFileLog("log.txt", "folger to old log", "filelog", golog.LogLevelDebug)
    /*
    в случае ошибки при создании файла возвращает ее, а также лог что пишет только в стандартный поток ошибок
    сама ошибка уже внесена в созданный лог
    туда также автоматически вносятся данные о создании лога (создание файла, его перемещение, отсутствие, создание папки для старых и новых логов)
    если по указаному пути файла его нет, то будет создан новый, если есть то старый будет перемещен в указаную директорию
    */
    if err!=nil{
        fileLog.Error(err)
    }
    fileLog.Info("hello file!")
}
```