package golog

import (
	"fmt"
	"io"
	"os"
	"path"
	"time"
)

// newLogFileWithSwap создает новый файл лога с перемещением предыдущего в указаную папку, возвращает интерфейс потока для записи
// интерфейс в случае успеха будет записывать данные в файл и стандартный поток иначе только в стандартный поток
// filename путь к файлу лога (после успешного вызова фнкции по этому пути будет новый файл)
// если такого файла не существует то функция укажет это в потоке информации
// toFolger папка куда перемещать старые логи (будет автоматически создана при отсутствии)
func newLogWriterWithSwap(filename string, toFolger string) (infoMes []string, w io.Writer, errMes error) {
	w = os.Stderr
	logFolger, logName := path.Split(filename)
	if stat, err := os.Lstat(toFolger); os.IsNotExist(err) {
		if err := os.MkdirAll(toFolger, os.ModePerm); err != nil {
			errMes = err
			return
		}
		infoMes = append(infoMes, fmt.Sprintf("folger %s create", toFolger))
	} else if !stat.IsDir() {
		errMes = fmt.Errorf("toFolger must be a folger")
		return
	}
	if stat, err := os.Lstat(filename); os.IsNotExist(err) {
		infoMes = append(infoMes, fmt.Sprintf("file %s is't exist", filename))
	} else if stat.IsDir() {
		errMes = fmt.Errorf("filename can't be a folger")
		return
	} else {
		newFilename := path.Join(toFolger, fmt.Sprintf("%s %s", time.Now().Format("2006-01-02 15-04-05"), logName))
		if err := os.Rename(filename, newFilename); err != nil {
			errMes = err
			return
		}
		infoMes = append(infoMes, "old log moved")
	}
	if _, err := os.Lstat(logFolger); os.IsNotExist(err) {
		if err := os.MkdirAll(toFolger, os.ModePerm); err != nil {
			errMes = err
			return
		}
		infoMes = append(infoMes, fmt.Sprintf("folger %s create", toFolger))
	}
	file, err := os.Create(filename)
	if err != nil {
		errMes = err
		return
	}
	infoMes = append(infoMes, fmt.Sprintf("file %s created", filename))
	w = io.MultiWriter(os.Stderr, file)
	return
}
