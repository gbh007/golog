package golog

import (
	"fmt"
	"io"
	"log"
	"os"
)

const (
	logFlags = log.Ldate | log.Ltime | log.Llongfile
	// LogLevelDebug уровень лога отладки
	LogLevelDebug = -100
	// LogLevelFull уровень полной информации из лога
	LogLevelFull = 0
	// LogLevelInfo уровень информирования
	LogLevelInfo = 100
	// LogLevelWarning уровень предупреждений
	LogLevelWarning = 300
	// LogLevelError уровень ошибок
	LogLevelError = 400
	// LogLevelCritical уровень критический
	LogLevelCritical = 500
	// LogLevelPanic уровень паники
	LogLevelPanic = 999
)

var stdLog = NewWriterLog(os.Stderr, "", LogLevelDebug)

// NewWriterLog функция для создания нового объекта лога
func NewWriterLog(out io.Writer, prefix string, level int) *Log {
	return &Log{
		logger:       log.New(out, prefix, logFlags),
		LogLevel:     level,
		LogFilename:  "log/log.log",
		OldLogFolger: "log/old",
		out:          out,
		flags:        logFlags,
	}
}

// NewFileLog функция для создания нового объекта лога с логированием в файл
// если файл лога существует перемещает его в указаную папку
// при возникновении ошибок пишет их в поток нового лога
// filename путь к файлу лога (новому или сущуствуещему)
// logOldFolger папка для хранения старых логов
func NewFileLog(filename, logOldFolger, prefix string, level int) (*Log, error) {
	infos, out, err := newLogWriterWithSwap(filename, logOldFolger)
	l := &Log{
		logger:       log.New(out, prefix, logFlags),
		LogLevel:     level,
		LogFilename:  filename,
		OldLogFolger: logOldFolger,
		out:          out,
		flags:        logFlags,
	}
	for _, mess := range infos {
		l.Info(mess)
	}
	if err != nil {
		l.Error(err)
		return l, err
	}
	return l, nil
}

// SetOutput устанавливает поток вывода данных стандартного лога
func SetOutput(out io.Writer) {
	stdLog.logger.SetOutput(out)
	stdLog.out = out
}

// SetFlags устанавливает флаги логгера стандартного лога
func SetFlags(flag int) {
	stdLog.logger.SetFlags(flag)
	stdLog.flags = flag
}

// New функция для создания нового лога по параметрам стандартного лога
func New(prefix string) *Log {
	return &Log{
		logger:       log.New(stdLog.out, prefix, stdLog.flags),
		LogLevel:     stdLog.LogLevel,
		LogFilename:  stdLog.LogFilename,
		OldLogFolger: stdLog.OldLogFolger,
		out:          stdLog.out,
		flags:        stdLog.flags,
	}
}

// Debug вывести данные на уровне отладки в стандартный лог
func Debug(v ...interface{}) bool {
	return stdLog.toLog(LogLevelDebug, "(DEBUG) ", v...)
}

// Info вывести данные на уровне информирования в стандартный лог
func Info(v ...interface{}) bool {
	return stdLog.toLog(LogLevelInfo, "(INFO) ", v...)
}

// Warning вывести данные на уровне предупреждений в стандартный лог
func Warning(v ...interface{}) bool {
	return stdLog.toLog(LogLevelWarning, "(WARNING) ", v...)
}

// Error вывести данные на уровне ошибки в стандартный лог
func Error(v ...interface{}) bool {
	return stdLog.toLog(LogLevelError, "(ERROR) ", v...)
}

// Critical вывести данные на критическом уровне в стандартный лог
func Critical(v ...interface{}) bool {
	return stdLog.toLog(LogLevelCritical, "(CRITICAL) ", v...)
}

// Panic вывести данные на уровне паники в стандартный лог
// при вызове бросает панику
func Panic(v ...interface{}) {
	stdLog.toLog(LogLevelPanic, "(PANIC) ", v...)
	panic(fmt.Sprintln(v...))
}
