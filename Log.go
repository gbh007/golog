package golog

import (
	"fmt"
	"io"
	"log"
)

// Log логгер с урпавлением вывода логов по уровню
type Log struct {
	logger       *log.Logger
	LogLevel     int
	LogFilename  string
	OldLogFolger string
	out          io.Writer
	flags        int
}

// SetOutput устанавливает поток вывода данных
func (l *Log) SetOutput(out io.Writer) {
	l.logger.SetOutput(out)
	l.out = out
}

// SetFlags устанавливает флаги логгера
func (l *Log) SetFlags(flag int) {
	l.logger.SetFlags(flag)
	l.flags = flag
}

// NewLog функция для создания нового лога по параметрам текущего за исключением префиксной строки
func (l *Log) NewLog(prefix string) *Log {
	return &Log{
		logger:       log.New(l.out, prefix, l.flags),
		LogLevel:     l.LogLevel,
		LogFilename:  l.LogFilename,
		OldLogFolger: l.OldLogFolger,
		out:          l.out,
		flags:        l.flags,
	}
}

func (l *Log) toLog(lv int, prefix string, v ...interface{}) bool {
	if lv < l.LogLevel {
		return false
	}
	if err := l.logger.Output(3, prefix+fmt.Sprintln(v...)); err != nil {
		return false
	}
	return true
}

// Debug вывести данные на уровне отладки
func (l *Log) Debug(v ...interface{}) bool {
	return l.toLog(LogLevelDebug, "(DEBUG) ", v...)
}

// Info вывести данные на уровне информирования
func (l *Log) Info(v ...interface{}) bool {
	return l.toLog(LogLevelInfo, "(INFO) ", v...)
}

// Warning вывести данные на уровне предупреждений
func (l *Log) Warning(v ...interface{}) bool {
	return l.toLog(LogLevelWarning, "(WARNING) ", v...)
}

// Error вывести данные на уровне ошибки
func (l *Log) Error(v ...interface{}) bool {
	return l.toLog(LogLevelError, "(ERROR) ", v...)
}

// Critical вывести данные на критическом уровне
func (l *Log) Critical(v ...interface{}) bool {
	return l.toLog(LogLevelCritical, "(CRITICAL) ", v...)
}

// Panic вывести данные на уровне паники
// при вызове бросает панику
func (l *Log) Panic(v ...interface{}) {
	l.toLog(LogLevelPanic, "(PANIC) ", v...)
	panic(fmt.Sprintln(v...))
}
